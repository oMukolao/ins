import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NameEdit extends StatefulWidget {
  @override
  _NameEditState createState() => _NameEditState();
}

class _NameEditState extends State<NameEdit> {
  var editingController = TextEditingController();
  bool useEdit = false;
  var name = 'Darian Gipich';
  void startEditing() {
    setState(() {
      if (editingController.text != null && editingController.text != '') {
        name = editingController.text;
      }
      useEdit = !useEdit;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: useEdit == false
          ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text(
                    name,
                    style: TextStyle(
                      color: Color.fromRGBO(68, 67, 71, 1),
                      fontSize: 16.sp,
                    ),
                  ),
                ),
                Container(
                  width: 20.w,
                  height: 20.w,
                  child: InkWell(
                    onTap: startEditing,
                    child: Image.asset('assets/images/pen.png'),
                  ),
                ),
              ],
            )
          : TextField(
              onEditingComplete: startEditing,
              controller: editingController,
            ),
    );
  }
}
