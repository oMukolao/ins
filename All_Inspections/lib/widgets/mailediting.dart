import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_form_builder/flutter_form_builder.dart';

class MailEdit extends StatefulWidget {
  @override
  _MailEditState createState() => _MailEditState();
}

class _MailEditState extends State<MailEdit> {
  var textEditing = TextEditingController();
  bool useEdit = false;
  var mail = 'dgipich@gmail.com';
  void startEditing() {
    setState(() {
      if (textEditing.text != null &&
          textEditing.text != '' &&
          textEditing.text.contains('@')) {
        mail = textEditing.text;
      }
      useEdit = !useEdit;
    });
  }

  // final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: useEdit == false
          ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text(
                    mail,
                    style: TextStyle(
                      color: Color.fromRGBO(68, 67, 71, 1),
                      fontSize: 16.sp,
                    ),
                  ),
                ),
                Container(
                  width: 20.w,
                  height: 20.w,
                  child: InkWell(
                    onTap: startEditing,
                    child: Image.asset('assets/images/pen.png'),
                  ),
                ),
              ],
            )
          : TextFormField(
              // validator: (value) {
              //   if (value.isEmpty || !value.contains('@')) {
              //     return 'invalid email';
              //   }
              //   return null;
              // },
              // onSaved: (value) {},
              keyboardType: TextInputType.emailAddress,
              onEditingComplete: startEditing,
              controller: textEditing,
            ),
    );
  }
}
