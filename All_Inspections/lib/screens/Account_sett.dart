import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../widgets/nameedit.dart';
import '../widgets/mailediting.dart';
import 'package:image_picker/image_picker.dart';

class AccountSettings extends StatefulWidget {
  static const routeName = '/account_settings';

  @override
  _AccountSettingsState createState() => _AccountSettingsState();
}

class _AccountSettingsState extends State<AccountSettings> {
  File _image;
  String images = 'assets/images/persSet.png';
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        designSize: Size(375, 667), allowFontScaling: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(207, 39, 44, 1),
        title: Center(
          child: Text(
            'Profile',
            style: TextStyle(
              fontSize: 18.sp,
              fontFamily: 'Encode Sans',
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        actions: [
          SizedBox(
            width: 50.w,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 20.h,
            ),
            // person
            Center(
              child: Container(
                width: 80.w,
                height: 80.w,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Color.fromRGBO(222, 219, 230, 1),
                    width: 3.w,
                  ),
                  borderRadius: BorderRadius.circular(50),
                ),
                child: Center(
                  child: _image == null
                      ? Container(
                          width: 50.w,
                          height: 50.w,
                          child: new Image.asset(
                            images,
                          ),
                        )
                      : Container(
                          width: 80.w,
                          height: 80.w,
                          child: new CircleAvatar(
                            backgroundImage: new FileImage(
                              _image,
                            ),
                          ),
                        ),
                ),
              ),
            ),
            SizedBox(
              height: 20.h,
            ),
            // add photo
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: getImage,
                    child: Row(
                      children: [
                        Container(
                          width: 20.w,
                          height: 18.w,
                          child: Image.asset('assets/images/addPhoto.png'),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        Container(
                          child: Text(
                            'Add photo',
                            style: TextStyle(
                              fontSize: 18.sp,
                              fontFamily: 'Encode Sans',
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 50.h,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  // full name
                  Container(
                    child: Row(
                      children: [
                        Text(
                          'Full name',
                          style: TextStyle(
                            color: Color.fromRGBO(126, 131, 142, 1),
                            fontSize: 12.sp,
                            fontFamily: 'Encode Sans',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  // name
                  NameEdit(),
                  SizedBox(
                    height: 30.h,
                  ),
                  // email text
                  Row(
                    children: [
                      Container(
                        child: Text(
                          'Email',
                          style: TextStyle(
                            color: Color.fromRGBO(126, 131, 142, 1),
                            fontSize: 12.sp,
                            fontFamily: 'Encode Sans',
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  // @
                  MailEdit(),
                ],
              ),
            ),
            SizedBox(
              height: 50.h,
            ),
            Divider(
              height: 1,
            ),
            SizedBox(
              height: 31.h,
            ),
            // change password
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: InkWell(
                      onTap: () {},
                      child: Text(
                        'Change password',
                        style: TextStyle(
                          fontSize: 18.sp,
                          fontFamily: 'Encode Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 20.w,
                    height: 20.w,
                    child: InkWell(
                      onTap: () {},
                      child: Image.asset('assets/images/arrow.png'),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 32.h,
            ),
            // settings
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: InkWell(
                      onTap: () {},
                      child: Text(
                        'Settings',
                        style: TextStyle(
                          fontSize: 18.sp,
                          fontFamily: 'Encode Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 20.w,
                    height: 20.w,
                    child: InkWell(
                      onTap: () {},
                      child: Image.asset('assets/images/arrow.png'),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 31.h,
            ),
            Divider(
              height: 1,
            ),
            SizedBox(
              height: 30.h,
            ),
            // logout
            Padding(
              padding: EdgeInsets.only(left: 24.w),
              child: Row(
                children: [
                  Container(
                    width: 20.w,
                    height: 20.w,
                    child: InkWell(
                      onTap: () {},
                      child: Image.asset('assets/images/logout.png'),
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  Container(
                    child: InkWell(
                      onTap: () {},
                      child: Text(
                        'Logout',
                        style: TextStyle(
                          fontSize: 18.sp,
                          fontFamily: 'Encode Sans',
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
