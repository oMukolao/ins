// import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_icons/flutter_icons.dart';

class Inscpections extends StatefulWidget {
  @override
  _InscpectionsState createState() => _InscpectionsState();
}

class _InscpectionsState extends State<Inscpections> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context,
        designSize: Size(375, 667), allowFontScaling: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(207, 39, 44, 1),
        leadingWidth: 55.w,
        leading: Row(
          children: [
            SizedBox(
              width: 24.w,
            ),
            Container(
              width: 30.w,
              height: 30.h,
              // margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
              ),
              child: InkWell(
                onTap: () {
                  Navigator.pushNamed(context, '/account_settings');
                },
                child: Container(
                  padding: EdgeInsets.all(7),
                  child: Image.asset(
                    'assets/images/person1.png',

                    // width: 24.w,
                    // height: 24.w,
                  ),
                ),
              ),
            ),
          ],
        ),
        title: Center(
          child: Text(
            'Inspections',
            style: TextStyle(
              fontFamily: 'Encode Sans',
              fontWeight: FontWeight.w500,
              fontSize: 18.sp,
              color: Colors.white,
            ),
          ),
        ),
        actions: [
          Container(
            child: InkWell(
              onTap: () {},
              child: Container(
                width: 22.w,
                height: 22.w,
                child: Image.asset('assets/images/filter.png'),
              ),
            ),
          ),
          SizedBox(
            width: 30.w,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 20.h,
            ),
            // Address
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(),
                    child: Text(
                      'ADDRESS',
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w500,
                        fontSize: 12.sp,
                        color: Color.fromRGBO(126, 131, 142, 1),
                      ),
                    ),
                  ),
                  // SizedBox(
                  //   width: 140.w,
                  // ),
                  Container(
                    width: 147,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          // margin: EdgeInsets.only(right: 3),
                          child: Text(
                            'PAYMENT',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                              fontFamily: 'Encode Sans',
                              fontWeight: FontWeight.w500,
                              fontSize: 12.sp,
                              color: Color.fromRGBO(126, 131, 142, 1),
                            ),
                          ),
                        ),
                        // SizedBox(
                        //   width: 30.w,
                        // ),
                        Container(
                          child: Text(
                            'STATUS',
                            style: TextStyle(
                              fontFamily: 'Encode Sans',
                              fontWeight: FontWeight.w500,
                              fontSize: 12.sp,
                              color: Color.fromRGBO(126, 131, 142, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Divider(
              height: 1.h,
              color: Color.fromRGBO(222, 219, 230, 1),
            ),
            // 1st
            buildPadding(
              '345 Brown Ave',
              'assets/images/checkGreen.png',
              'Closed',
              '8 Apr, 2020',
            ),
            // 2nd
            buildPadding('345 Brown Ave', 'assets/images/checkGray.png', 'Open',
                '8 Apr, 2020'),

            // 3rd
            buildPadding('345 Brown Avee', 'assets/images/checkGray.png',
                'IN PROGRESS', '10 Apr, 2020'),
            // 4
            buildPadding(
              '345 Brown Ave',
              'assets/images/checkGreen.png',
              'Closed',
              '8 Apr, 2020',
            ),
            // 5
            buildPadding('345 Brown Ave', 'assets/images/checkGray.png', 'Open',
                '8 Apr, 2020'),

            // 6
            buildPadding('345 Brown Avee', 'assets/images/checkGray.png',
                'IN PROGRESS', '10 Apr, 2020'),
          ],
        ),
      ),
    );
  }

  Column buildPadding(String adress, String img, String status, String data) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                height: 22.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    // width: 200.w,
                    child: Text(
                      adress,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w500,
                        fontSize: 18.sp,
                        color: Color.fromRGBO(68, 67, 71, 1),
                      ),
                    ),
                  ),
                  Container(
                    width: 117.w,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Image.asset(img),
                        ),
                        Container(
                          child: status == 'Open'
                              ? Text(
                                  status,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                  style: TextStyle(
                                    color: Color.fromRGBO(235, 95, 93, 1),
                                    fontFamily: 'Encode Sans',
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12.sp,
                                  ),
                                )
                              : status == 'IN PROGRESS'
                                  ? Text(
                                      status,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: TextStyle(
                                        color: Color.fromRGBO(250, 188, 116, 1),
                                        fontFamily: 'Encode Sans',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12.sp,
                                      ),
                                    )
                                  : Text(
                                      status,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: TextStyle(
                                        color: Color.fromRGBO(35, 159, 149, 1),
                                        fontFamily: 'Encode Sans',
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12.sp,
                                      ),
                                    ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    // width: 200.w,
                    child: Text(
                      'John Smith',
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w300,
                        fontSize: 16.sp,
                        color: Color.fromRGBO(68, 67, 71, 1),
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      data,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(
                        fontFamily: 'Encode Sans',
                        fontWeight: FontWeight.w300,
                        fontSize: 12.sp,
                        color: Color.fromRGBO(126, 131, 142, 1),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Divider(
          height: 1.h,
          color: Color.fromRGBO(222, 219, 230, 1),
        ),
      ],
    );
  }
}
