import 'package:flutter/material.dart';
import './screens/Inspections.dart';
import './screens/Account_sett.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Inspections',
      home: Inscpections(),
      routes: {
        AccountSettings.routeName: (ctx) => AccountSettings(),
      },
    );
  }
}
